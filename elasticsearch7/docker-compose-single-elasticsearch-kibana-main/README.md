# 修改目录权限
```
chmod -R 777 ./esdata
chmod -R 777 ./plugin
```
# 安装 elasticsearch 插件ik

```
# 进入宿主机的plugins目录（容器的/usr/share/elasticsearch/plugins）
cd /plugin
 
# 创建 ik 目录，并进入
mkdir ik
cd ik
 
# 下载ik分词器
wget https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.5.1/elasticsearch-analysis-ik-7.5.1.zip
 
# 具体的文件名可以使用ls看一下
unzip elasticsearch-analysis-ik-7.5.1.zip
 
# 删除 elasticsearch-analysis-ik-7.5.1.zip
rm -rf elasticsearch-analysis-ik-7.5.1.zip
 
# 重启服务
docker-compose stop
docker-compose up -d
```


